# Money transfer Rest API

A Java RESTful API for money transfers between users accounts

### Technologies
- JAX-RS API
- H2 in memory database
- Logback
- Jetty Container (for Test and Demo app)
- Apache HTTP Client


### How to run
```sh
mvn exec:java
```

Application starts a jetty server on localhost port 8080 An H2 in memory database initialized with some sample user and account data To view

- http://localhost:8080/user/1
- http://localhost:8080/user/2
- http://localhost:8080/user/3
- http://localhost:8080/user/4
- http://localhost:8080/account/1
- http://localhost:8080/account/2
- http://localhost:8080/account/3
- http://localhost:8080/account/4
- http://localhost:8080/account/5

### Available Services

| HTTP METHOD | PATH | USAGE |
| -----------| ------ | ------ |
| GET | /user/{userId} | get user by user ID | 
| GET | /user/all | get all users | 
| PUT | /user/create | create a new user | 
| POST | /user/{userId} | update user | 
| DELETE | /user/{userId} | deactivate user and his accounts | 
| GET | /account/{accountId} | get account by accountId | 
| GET | /account/{userId}/all | get all user accounts | 
| GET | /account/{accountId}/balance | get account balance by accountId | 
| PUT | /account/create | create a new account
| DELETE | /account/{accountId} | deactivate account | 
| PUT | /account/{accountId}/decrease/{amount} | withdraw money from account | 
| PUT | /account/{accountId}/increase/{amount} | deposit money to account | 
| POST | /transaction | perform transaction between 2 user accounts | 

### Http Status
- 200 OK: The request has succeeded
- 400 Bad Request: The request could not be understood by the server 
- 404 Not Found: The requested resource cannot be found
- 500 Internal Server Error: The server encountered an unexpected condition 

### Sample JSON for User and Account
##### User : 
```sh
{
        "userId": "1",
        "name": "name_1",
        "surName": "sur_1",
        "middleName": "midl_1",
        "phone": "112",
        "email": "ab@email.com",
        "active": true
    },
    {
        "userId": "2",
        "name": "name_2",
        "surName": "sur_2",
        "middleName": "midl_2",
        "phone": "113",
        "email": "bc@email.com",
        "active": true
    },
    {
        "userId": "3",
        "name": "name_3",
        "surName": "sur_3",
        "middleName": null,
        "phone": "114",
        "email": "cd@email.com",
        "active": true
    },
    {
        "userId": "4",
        "name": "name_4",
        "surName": "sur_4",
        "middleName": null,
        "phone": "115",
        "email": "de@email.com",
        "active": true
    }
```
##### User Account: : 

```sh
        {
            "accountId": "1",
            "user": "1",
            "balance": 5000,
            "active": true
        },
        {
            "accountId": "2",
            "user": "1",
            "balance": 3050,
            "active": true
        }
```

