package com.companyname.transfer.dao;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.User;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by zatsepilin on 04.05.2018.
 */
public class TestUserDAO {
	
	private static final DAOFactory h2DaoFactory = DAOFactory.getDAOFactory();

	@BeforeClass
	public static void setup() {
		// prepare test database and test data. Test data are initialised from
		// src/test/resources/initial.sql
		h2DaoFactory.generateData();
	}

	@Test
	public void testGetAllUsers() throws CustomException {
		List<User> allUsers = h2DaoFactory.getUserDAO().findAllActiveUsers();
		assertTrue(allUsers.size() > 1);
	}

	@Test
	public void testGetUserById() throws CustomException {
		User user = h2DaoFactory.getUserDAO().findUserById(("2"));
		assertTrue(user.getName().equals("name_2"));
	}

	@Test
	public void testGetNonExistedUserById() throws CustomException {
		User user = h2DaoFactory.getUserDAO().findUserById("555");
		assertTrue(user == null);
	}

	@Test
	public void testCreateUser() throws CustomException {
		User user = new User("name_10", "sur_10", "middle_10", "911", "custom@email.com",true);
		int createdCount = h2DaoFactory.getUserDAO().insertUser(user);
		assertTrue(createdCount == 1);
	}

	@Test
	public void testUpdateUser() throws CustomException {
		User user = new User("3","name_33","sur_33", "middle_33", "1143","test2@gmail.com3",true);
		int rowCount = h2DaoFactory.getUserDAO().updateUser(user, "3");
		// assert one row(user) updated
		assertTrue(rowCount == 1);
		assertTrue(h2DaoFactory.getUserDAO().findUserById("3").getPhone().equals("1143"));
	}

	@Test
	public void testUpdateNonExistedUser() throws CustomException {
		User user = new User("999","name_33", "sur_33", "1143","test2@gmail.com3",true);
		int rowCount = h2DaoFactory.getUserDAO().updateUser(user, "999");
		// assert one row(user) updated
		assertTrue(rowCount == 0);
	}

	@Test
	public void testDeleteUser() throws CustomException {
		int rowCount = h2DaoFactory.getUserDAO().deactivateUser("1");
		// assert one row(user) deleted
		assertTrue(rowCount == 1);
		// assert user no longer there
		assertTrue(h2DaoFactory.getUserDAO().findUserById("1") == null);
	}

	@Test
	public void testDeleteNonExistingUser() throws CustomException {
		int rowCount = h2DaoFactory.getUserDAO().deactivateUser("0");
		// assert no row(user) deleted
		assertTrue(rowCount == 0);

	}

}
