package com.companyname.transfer.dao;


import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.BankingAccount;
import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by zatsepilin on 04.05.2018.
 */
public class TestAccountDAO {

	private static final DAOFactory h2DaoFactory = DAOFactory.getDAOFactory();

	@BeforeClass
	public static void setup() {
		// prepare test database and test data. Test data are initialised from
		// src/test/resources/initial.sql
		h2DaoFactory.generateData();
	}


	@Test
	public void testGetAccountById() throws CustomException {
		BankingAccount account = h2DaoFactory.getBankingAccountDAO().findAccountById("2");
		assertTrue(account.getUser().equals("1"));
		assertTrue(account.getBalance().equals(new BigDecimal(3050).setScale(4, RoundingMode.HALF_EVEN)));
	}

	@Test
	public void testGetNonExistingAccById() throws CustomException {
		BankingAccount account = h2DaoFactory.getBankingAccountDAO().findAccountById("999");
		assertTrue(account == null);
	}

	@Test
	public void testCreateAccount() throws CustomException {
		BigDecimal balance = new BigDecimal(11).setScale(4, RoundingMode.HALF_EVEN);
		BankingAccount account = new BankingAccount("2", balance, true);
		int createdCount = h2DaoFactory.getBankingAccountDAO().insertAccount(account);
		assertTrue(createdCount == 1);
	}
	@Test
	public void testCreateAccountWithNotExistedUser() throws CustomException {
		String exceptionMessage = StringUtils.EMPTY;
		BigDecimal balance = new BigDecimal(11).setScale(4, RoundingMode.HALF_EVEN);
		BankingAccount account = new BankingAccount("myUser", balance, true);
		try {
			h2DaoFactory.getBankingAccountDAO().insertAccount(account);
		} catch (CustomException exception) {
			exceptionMessage = exception.getMessage();
		}

		assertTrue(exceptionMessage.contains("Error creating account"));
	}

	@Test
	public void testDeleteAccount() throws CustomException {
		int rowCount = h2DaoFactory.getBankingAccountDAO().deactivateAccount("2");
		// assert one row(user) deleted
		assertTrue(rowCount == 1);
		// assert user no longer there
		assertTrue(h2DaoFactory.getBankingAccountDAO().findAccountById("2") == null);
	}

	@Test
	public void testDeleteNonExistingAccount() throws CustomException {
		int rowCount = h2DaoFactory.getBankingAccountDAO().deactivateAccount("999");
		// assert no row(user) deleted
		assertTrue(rowCount == 0);

	}

	@Test
	public void testUpdateAccountBalanceSufficientFund() throws CustomException {

		BigDecimal deltaDeposit = new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN);
		BigDecimal afterDeposit = new BigDecimal(5050).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdated = h2DaoFactory.getBankingAccountDAO().updateBalance("1", deltaDeposit);
		assertTrue(rowsUpdated == 1);
		assertTrue(h2DaoFactory.getBankingAccountDAO().findAccountById("1").getBalance().equals(afterDeposit));
		BigDecimal deltaWithDraw = new BigDecimal(-50).setScale(4, RoundingMode.HALF_EVEN);
		BigDecimal afterWithDraw = new BigDecimal(5000).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdatedW = h2DaoFactory.getBankingAccountDAO().updateBalance("1", deltaWithDraw);
		assertTrue(rowsUpdatedW == 1);
		assertTrue(h2DaoFactory.getBankingAccountDAO().findAccountById("1").getBalance().equals(afterWithDraw));

	}

	@Test(expected = CustomException.class)
	public void testUpdateAccountBalanceNotEnoughFund() throws CustomException {
		BigDecimal deltaWithDraw = new BigDecimal(-50000).setScale(4, RoundingMode.HALF_EVEN);
		int rowsUpdatedW = h2DaoFactory.getBankingAccountDAO().updateBalance("1", deltaWithDraw);
		assertTrue(rowsUpdatedW == 0);

	}

}