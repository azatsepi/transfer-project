package com.companyname.transfer.dao;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.BankingAccount;
import com.companyname.transfer.model.Transaction;
import org.apache.commons.dbutils.DbUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CountDownLatch;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by zatsepilin on 04.05.2018.
 */
public class TestAccountBalance {

	private static Logger log = LoggerFactory.getLogger(TestAccountDAO.class);
	private static final DAOFactory h2DaoFactory = DAOFactory.getDAOFactory();
	private static final int THREADS_COUNT = 100;

	@BeforeClass
	public static void setup() {
		// prepare test database and test data, Test data are initialised from
		// src/test/resources/initial.sql
		h2DaoFactory.generateData();
	}


	@Test
	public void testAccountSingleThreadSameCcyTransfer() throws CustomException {

		final BankingAccountDAO accountDAO = h2DaoFactory.getBankingAccountDAO();

		BigDecimal transferAmount = new BigDecimal(50.01234).setScale(4, RoundingMode.HALF_EVEN);

		Transaction transaction = new Transaction(transferAmount, "3", "4");

		long startTime = System.currentTimeMillis();

		accountDAO.transferAccountBalance(transaction);
		long endTime = System.currentTimeMillis();

		log.info("TransferAccountBalance finished, time taken: " + (endTime - startTime) + "ms");

		BankingAccount accountFrom = accountDAO.findAccountById("3");

		BankingAccount accountTo = accountDAO.findAccountById("4");

		log.debug("Account From: " + accountFrom);

		log.debug("Account From: " + accountTo);

		assertTrue(accountFrom.getBalance().compareTo(new BigDecimal(49.9877).setScale(4, RoundingMode.HALF_EVEN)) == 0);
		assertTrue(accountTo.getBalance().equals(new BigDecimal(1051.0123).setScale(4, RoundingMode.HALF_EVEN)));

	}

	@Test
	public void testAccountMultiThreadedTransfer() throws InterruptedException, CustomException {
		final BankingAccountDAO accountDAO = h2DaoFactory.getBankingAccountDAO();

		final CountDownLatch latch = new CountDownLatch(THREADS_COUNT);
		for (int i = 0; i < THREADS_COUNT; i++) {
			new Thread(() -> {
                try {
                    Transaction transaction = new Transaction(
                            new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN), "1", "2");
                    accountDAO.transferAccountBalance(transaction);
                } catch (Exception e) {
                    log.error("Error occurred during transfer ", e);
                } finally {
                    latch.countDown();
                }
            }).start();
		}

		latch.await();

		BankingAccount accountFrom = accountDAO.findAccountById("1");

		BankingAccount accountTo = accountDAO.findAccountById("2");

		log.debug("Account From: " + accountFrom);

		log.debug("Account From: " + accountTo);

		assertTrue(accountFrom.getBalance().equals(new BigDecimal(0).setScale(4, RoundingMode.HALF_EVEN)));
		assertTrue(accountTo.getBalance().equals(new BigDecimal(8050).setScale(4, RoundingMode.HALF_EVEN)));

	}

	@Test
	public void testTransferFailOnDBLock() throws CustomException, SQLException {
		final String SQL_LOCK_ACC = "SELECT * FROM Account WHERE accountId = 5 FOR UPDATE";
		Connection conn = null;
		PreparedStatement lockStmt = null;
		ResultSet rs = null;
		BankingAccount fromAccount = null;

		try {
			conn = H2DAOFactory.getConnection();
			conn.setAutoCommit(false);
			// lock account for writing:
			lockStmt = conn.prepareStatement(SQL_LOCK_ACC);
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				fromAccount = new BankingAccount(rs.getString("accountId"), rs.getString("user"),
						rs.getBigDecimal("balance"), rs.getBoolean("active"));
				log.debug("Locked Account: " + fromAccount);
			}

			if (fromAccount == null) {
				throw new CustomException("Locking error during test, SQL = " + SQL_LOCK_ACC);
			}

			BigDecimal transferAmount = new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN);

			Transaction transaction = new Transaction(transferAmount, "5", "6");
			h2DaoFactory.getBankingAccountDAO().transferAccountBalance(transaction);
			conn.commit();
		} catch (Exception e) {
			log.error("Exception occurred, initiate a rollback");
			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException re) {
				log.error("Fail to rollback transaction", re);
			}
		} finally {
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(lockStmt);
		}

		// now inspect account 3 and 4 to verify no transaction occurred
		BigDecimal originalBalance = new BigDecimal(1001).setScale(4, RoundingMode.HALF_EVEN);
		assertTrue(h2DaoFactory.getBankingAccountDAO().findAccountById("5").getBalance().equals(originalBalance));
		assertTrue(h2DaoFactory.getBankingAccountDAO().findAccountById("6").getBalance().equals(originalBalance));
	}

}
