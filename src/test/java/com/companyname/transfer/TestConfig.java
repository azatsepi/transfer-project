package com.companyname.transfer;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class TestConfig {
    public static final String HOST = System.getProperty("transfer.host", "localhost");
    public static final String PORT = System.getProperty("transfer.port", "8081");
}
