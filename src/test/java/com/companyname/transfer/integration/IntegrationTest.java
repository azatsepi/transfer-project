package com.companyname.transfer.integration;

import com.companyname.transfer.TestConfig;
import com.companyname.transfer.dao.DAOFactory;
import com.companyname.transfer.rest.BankingAccountController;
import com.companyname.transfer.rest.ErrorHandler;
import com.companyname.transfer.rest.TransactionController;
import com.companyname.transfer.rest.UserController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * Created by zatsepilin on 04.05.2018.
 */
public abstract class IntegrationTest {

    protected static Server server = null;
    protected static PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();

    protected static HttpClient client ;
    protected static DAOFactory h2DaoFactory = DAOFactory.getDAOFactory();
    protected ObjectMapper mapper = new ObjectMapper();
    protected URIBuilder builder = new URIBuilder().setScheme("http").setHost(TestConfig.HOST).setPort(Integer.parseInt(TestConfig.PORT));


    @BeforeClass
    public static void setup() throws Exception {
        h2DaoFactory.generateData();
        startServer();
        connManager.setDefaultMaxPerRoute(100);
        connManager.setMaxTotal(200);
        client = HttpClients.custom()
                .setConnectionManager(connManager)
                .setConnectionManagerShared(true)
                .build();

    }

    @AfterClass
    public static void closeClient() {
        //server.stop();
        HttpClientUtils.closeQuietly(client);
    }


    private static void startServer() throws Exception {
        if (server == null) {
            server = new Server(Integer.parseInt(TestConfig.PORT));
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setContextPath("/");
            server.setHandler(context);
            ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
            servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                    UserController.class.getCanonicalName() + "," +
                            BankingAccountController.class.getCanonicalName() + "," +
                            ErrorHandler.class.getCanonicalName() + "," +
                            TransactionController.class.getCanonicalName());
            server.start();
        }
    }
}
