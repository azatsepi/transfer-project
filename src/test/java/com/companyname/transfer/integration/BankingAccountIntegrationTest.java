package com.companyname.transfer.integration;

import com.companyname.transfer.model.BankingAccount;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by zatsepilin on 04.05.2018.
 */
public class BankingAccountIntegrationTest extends IntegrationTest {

    @Test
    public void testGetAllAccountByUser() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/1/all").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);

        String jsonString = EntityUtils.toString(response.getEntity());
        List<BankingAccount> users = mapper.readValue(jsonString, new TypeReference<List<BankingAccount>>(){});
        assertTrue(users.size() > 1);
    }

    @Test
    public void testGetAccountBalance() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/4/balance").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
        //check the content, assert user test2 have balance 100
        String balance = EntityUtils.toString(response.getEntity());
        BigDecimal result = new BigDecimal(balance).setScale(4, RoundingMode.HALF_EVEN);
        BigDecimal expecxted = new BigDecimal(1001).setScale(4, RoundingMode.HALF_EVEN);
        assertTrue(expecxted.equals(result));
    }

    @Test
    public void testCreateAccount() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/create").build();
        BigDecimal balance = new BigDecimal(100).setScale(4, RoundingMode.HALF_EVEN);
        BankingAccount acc = new BankingAccount("3", balance,true);
        String jsonInString = mapper.writeValueAsString(acc);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = new HttpPost(uri);
        request.setHeader("Content-type", "application/json");
        request.setEntity(entity);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
    }



    @Test
    public void testDeposit() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/3/increase/100").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
        String jsonString = EntityUtils.toString(response.getEntity());
        BankingAccount afterDeposit = mapper.readValue(jsonString, BankingAccount.class);

        assertTrue(afterDeposit.getBalance().equals(new BigDecimal(200).setScale(4, RoundingMode.HALF_EVEN)));

    }

    @Test
    public void testDecreaseWithSufficientCapital() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/2/decrease/50").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
        String jsonString = EntityUtils.toString(response.getEntity());
        BankingAccount afterDeposit = mapper.readValue(jsonString, BankingAccount.class);

        assertTrue(afterDeposit.getBalance().equals(new BigDecimal(3000).setScale(4, RoundingMode.HALF_EVEN)));

    }

    @Test
    public void testDecreaseWithNonSufficientCapital() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/3/decrease/1000.0001").build();
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        String responseBody = EntityUtils.toString(response.getEntity());
        assertTrue(statusCode == 500);
        assertTrue(responseBody.contains("Balance is not satisfied"));
    }

    @Test
    public void testDeactivateAccount() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/1").build();
        HttpDelete request = new HttpDelete(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 200);
    }

    @Test
    public void testDeactivateNotExistedAccount() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/15").build();
        HttpDelete request = new HttpDelete(uri);
        request.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == 404);
    }
}
