DROP TABLE IF EXISTS user;

CREATE TABLE user (
  userId     VARCHAR(30) PRIMARY KEY AUTO_INCREMENT NOT NULL,
  name       VARCHAR(20)                            NOT NULL,
  surName    VARCHAR(40)                            NOT NULL,
  middleName VARCHAR(20),
  phone      VARCHAR(40)                            NOT NULL,
  email      VARCHAR(120)                           NOT NULL,
  active     BOOLEAN                                NOT NULL
);

CREATE UNIQUE INDEX idx_user
  ON user (name, surName, phone, email, active);

DROP TABLE IF EXISTS bankingaccount;

CREATE TABLE bankingaccount (
  accountId VARCHAR(30) PRIMARY KEY AUTO_INCREMENT NOT NULL,
  user      VARCHAR(30)                            NOT NULL,
  balance   DECIMAL(15, 4)                         NOT NULL,
  active    BOOLEAN                                NOT NULL,
  FOREIGN KEY (user)
  REFERENCES public.user (userId)
);

CREATE UNIQUE INDEX idx_account
  ON bankingaccount (accountId, user, balance);

INSERT INTO user (name, surName, middleName, phone, email, active)
VALUES ('name_1', 'sur_1', 'midl_1', '112', 'ab@email.com', TRUE);
INSERT INTO bankingaccount (user, balance, active) VALUES ('1', 5000.0000, TRUE);
INSERT INTO bankingaccount (user, balance, active) VALUES ('1', 3050.0000, TRUE);

INSERT INTO user (name, surName, middleName,phone, email, active) VALUES ('name_2', 'sur_2', 'midl_2' ,'113', 'bc@email.com', TRUE);
INSERT INTO bankingaccount (user, balance, active) VALUES ('2', 100.0000, TRUE);

INSERT INTO user (name, surName, phone, email, active) VALUES ('name_3', 'sur_3', '114', 'cd@email.com', TRUE);
INSERT INTO bankingaccount (user, balance, active) VALUES ('3', 1001.0000, TRUE);
INSERT INTO bankingaccount (user, balance, active) VALUES ('3', 1001.0000, TRUE);
INSERT INTO bankingaccount (user, balance, active) VALUES ('3', 1001.0000, TRUE);
INSERT INTO user (name, surName, phone, email, active) VALUES ('name_4', 'sur_4', '115', 'de@email.com', TRUE);
