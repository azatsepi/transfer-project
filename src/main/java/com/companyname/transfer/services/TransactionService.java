package com.companyname.transfer.services;

import com.companyname.transfer.dao.DAOFactory;
import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.Transaction;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class TransactionService {
    private final DAOFactory daoFactory = DAOFactory.getDAOFactory();

    public int transferAccountBalance(Transaction transaction) throws CustomException {
        return daoFactory.getBankingAccountDAO().transferAccountBalance(transaction);
    }
}
