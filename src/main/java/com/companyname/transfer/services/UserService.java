package com.companyname.transfer.services;

import com.companyname.transfer.dao.DAOFactory;
import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.User;

import java.util.List;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class UserService {
    private final DAOFactory daoFactory = DAOFactory.getDAOFactory();

    public List<User> getAllUsers() throws CustomException {
       return daoFactory.getUserDAO().findAllActiveUsers();
    }

    public User getUserById(String userId) throws CustomException {
        return daoFactory.getUserDAO().findUserById(userId);
    }


    public int createUser(User user) throws CustomException {
       return daoFactory.getUserDAO().insertUser(user);
    }

    public int updateUser(User user, String userId) throws CustomException {
       return daoFactory.getUserDAO().updateUser(user,userId);
    }

    public int deactivateUser(String userId) throws CustomException {
        daoFactory.getBankingAccountDAO().deactivateAccountsByUser(userId);
        return daoFactory.getUserDAO().deactivateUser(userId);

    }
}
