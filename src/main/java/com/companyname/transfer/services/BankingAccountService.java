package com.companyname.transfer.services;

import com.companyname.transfer.dao.DAOFactory;
import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.BankingAccount;
import com.companyname.transfer.model.User;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class BankingAccountService {
    private final DAOFactory daoFactory = DAOFactory.getDAOFactory();

    public List<BankingAccount> getAllAccounts(String userId) throws CustomException {
        return daoFactory.getBankingAccountDAO().findAllBankingAccountsByUser(userId);
    }

    public BankingAccount getAccountById(String accountId) throws CustomException {
        return daoFactory.getBankingAccountDAO().findAccountById(accountId);
    }

    public int createAccount(BankingAccount account) throws CustomException {
        isUserExist(account.getUser());
        return daoFactory.getBankingAccountDAO().insertAccount(account);
    }

    public int deactivateAccount(String accountId) throws CustomException {
       return daoFactory.getBankingAccountDAO().deactivateAccount(accountId);
    }

    public int updateAccountBalance(String accountId, BigDecimal delta) throws CustomException {
       return daoFactory.getBankingAccountDAO().updateBalance(accountId, delta);
    }

    private boolean isUserExist(String userId) {
        try {
            User user = daoFactory.getUserDAO().findUserById(userId);
            return user == null ? false : true;
        } catch (CustomException e) {
            return false;
        }

    }
}
