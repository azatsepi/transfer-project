package com.companyname.transfer.dao.impl;

import com.companyname.transfer.dao.H2DAOFactory;
import com.companyname.transfer.dao.UserDAO;
import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.User;
import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.companyname.transfer.constans.SQLQueryConstans.User.*;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class UserDAOImpl implements UserDAO {
    private static Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);


    public List<User> findAllActiveUsers() throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<User> users = new ArrayList<>();
        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_ALL_USERS);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User(resultSet.getString("userId"),
                        resultSet.getString("name"),
                        resultSet.getString("surName"),
                        resultSet.getString("middleName"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getBoolean("active"));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new CustomException("SQL exception", e);
        } finally {
            DbUtils.closeQuietly(connection, preparedStatement, resultSet);
        }
    }

    public User findUserById(String userId) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_USER_BY_ID);
            preparedStatement.setString(1, userId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User(resultSet.getString("userId"),
                        resultSet.getString("name"),
                        resultSet.getString("surName"),
                        resultSet.getString("middleName"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getBoolean("active"));
            }
            return user;
        } catch (SQLException e) {
            throw new CustomException("SQL exception", e);
        } finally {
            DbUtils.closeQuietly(connection, preparedStatement, resultSet);
        }
    }

    public int insertUser(User user) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet generatedKeys = null;
        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_INSERT_USER);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurName());
            preparedStatement.setString(3, user.getMiddleName());
            preparedStatement.setString(4, user.getPhone());
            preparedStatement.setString(5, user.getEmail());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                logger.error("Cannot insert user: " + user);
                return affectedRows;
            }
            return affectedRows;
        } catch (SQLException e) {
            logger.error("Cannot insert user:" + user);
            throw new CustomException("Error creating user data", e);
        } finally {
            DbUtils.closeQuietly(connection, preparedStatement, generatedKeys);
        }
    }

    public int updateUser(User user, String userId) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurName());
            preparedStatement.setString(3, user.getMiddleName());
            preparedStatement.setString(4, user.getPhone());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setString(6, user.getUserId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error updating user :" + user);
            throw new CustomException("Error update user data", e);
        } finally {
            DbUtils.closeQuietly(connection);
            DbUtils.closeQuietly(preparedStatement);
        }
    }

    public int deactivateUser(String userId) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_DEACTIVATE_USER);
            preparedStatement.setString(1, userId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error deactivating user :" + userId);
            throw new CustomException("Error deactivating user :", e);
        } finally {
            DbUtils.closeQuietly(connection);
            DbUtils.closeQuietly(preparedStatement);
        }
    }
}
