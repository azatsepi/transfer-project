package com.companyname.transfer.dao.impl;

import com.companyname.transfer.dao.BankingAccountDAO;
import com.companyname.transfer.dao.H2DAOFactory;
import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.BankingAccount;
import com.companyname.transfer.model.Transaction;
import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.companyname.transfer.constans.SQLQueryConstans.Account.*;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class BankingAccountDAOImpl implements BankingAccountDAO {
    private static Logger logger = LoggerFactory.getLogger(BankingAccountDAO.class);


    public int updateBalance(String accountId, BigDecimal delta) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement updateStatement = null;
        ResultSet resultSet = null;
        BankingAccount targetAccount = null;
        int updateCount = -1;
        try {
            connection = new H2DAOFactory().getConnection();
            connection.setAutoCommit(false);
            //lock
            preparedStatement = connection.prepareStatement(SQL_LOCK_ACCOUNT_BY_ID);
            preparedStatement.setString(1, accountId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                targetAccount = new BankingAccount(resultSet.getString("accountId"), resultSet.getString("user"),
                        resultSet.getBigDecimal("balance"), resultSet.getBoolean("active"));
                logger.debug("Target account for updating: " + targetAccount);
            }

            if (targetAccount == null) {
                throw new CustomException("Fail to lock account : " + targetAccount);
            }
            // update
            BigDecimal balance = targetAccount.getBalance().add(delta);
            if (balance.compareTo(BigDecimal.ZERO) < 0) {
                throw new CustomException("Balance is not satisfied: " + targetAccount);
            }

            updateStatement = connection.prepareStatement(SQL_UPDATE_ACCOUNT_BALANCE);
            updateStatement.setBigDecimal(1, balance);
            updateStatement.setString(2, accountId);
            updateCount = updateStatement.executeUpdate();
            connection.commit();
            logger.debug("New balance was successfully updated " + targetAccount);
            return updateCount;
        } catch (SQLException exception) {
            // rollback
            logger.error("User Transaction Failed, rollback initiated for: " + accountId, exception);
            try {
                if (connection != null)
                    connection.rollback();
            } catch (SQLException re) {
                throw new CustomException("Fail to rollback transaction", re);
            }
        } finally {
            DbUtils.closeQuietly(connection);
            DbUtils.closeQuietly(resultSet);
            DbUtils.closeQuietly(preparedStatement);
            DbUtils.closeQuietly(updateStatement);
        }
        return updateCount;
    }

    public List<BankingAccount> findAllBankingAccountsByUser(String userId) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<BankingAccount> accounts = new ArrayList<>();
        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_ACCOUNTS_BY_USER);
            preparedStatement.setString(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                BankingAccount account = new BankingAccount(resultSet.getString("accountId"), resultSet.getString("user"),
                        resultSet.getBigDecimal("balance"), resultSet.getBoolean("active"));
                accounts.add(account);
            }
            return accounts;
        } catch (SQLException e) {
            throw new CustomException("SQL exception", e);
        } finally {
            DbUtils.closeQuietly(connection, preparedStatement, resultSet);
        }
    }

    public BankingAccount findAccountById(String accountId) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        BankingAccount account = null;
        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_ACCOUNT_BY_ID);
            preparedStatement.setString(1, accountId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                account = new BankingAccount(resultSet.getString("accountId"), resultSet.getString("user"),
                        resultSet.getBigDecimal("balance"), resultSet.getBoolean("active"));
                logger.debug("Account was found: " + account);
            }
            return account;
        } catch (SQLException e) {
            throw new CustomException("Error during account finding", e);
        } finally {
            DbUtils.closeQuietly(connection, preparedStatement, resultSet);
        }
    }

    public int insertAccount(BankingAccount account) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            conn = new H2DAOFactory().getConnection();
            stmt = conn.prepareStatement(SQL_CREATE_ACCOUNT);
            stmt.setString(1, account.getUser());
            stmt.setBigDecimal(2, account.getBalance());
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                logger.error("Creating account failed, no rows affected.");
            }
            return affectedRows;
        } catch (SQLException e) {
            logger.error("Error Inserting Account  " + account);
            throw new CustomException("Error creating account " + account, e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, generatedKeys);
        }
    }

    public int deactivateAccount(String accountId) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_DEACTIVATE_ACCOUNT_BY_ID);
            preparedStatement.setString(1, accountId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new CustomException("Error deactivating by accountId  " + accountId, e);
        } finally {
            DbUtils.closeQuietly(connection);
            DbUtils.closeQuietly(preparedStatement);
        }
    }

    public void deactivateAccountsByUser(String userId) throws CustomException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = new H2DAOFactory().getConnection();
            preparedStatement = connection.prepareStatement(SQL_DEACTIVATE_ACCOUNTS_BY_USER);
            preparedStatement.setString(1, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new CustomException("Error deactivating by userId  " + userId, e);
        } finally {
            DbUtils.closeQuietly(connection);
            DbUtils.closeQuietly(preparedStatement);
        }
    }

    public int transferAccountBalance(Transaction transaction) throws CustomException {
        int result = -1;
        Connection conn = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        ResultSet resultSet = null;
        BankingAccount fromAccount = null;
        BankingAccount toAccount = null;

        try {
            conn = new H2DAOFactory().getConnection();
            conn.setAutoCommit(false);
            // lock the credit and debit account for writing:
            lockStmt = conn.prepareStatement(SQL_LOCK_ACCOUNT_BY_ID);
            lockStmt.setString(1, transaction.getFromAccount());
            resultSet = lockStmt.executeQuery();
            if (resultSet.next()) {
                fromAccount = new BankingAccount(resultSet.getString("accountId"), resultSet.getString("user"),
                        resultSet.getBigDecimal("balance"), resultSet.getBoolean("active"));
                logger.debug("transferAccountBalance from Account: " + fromAccount);
            }
            lockStmt = conn.prepareStatement(SQL_LOCK_ACCOUNT_BY_ID);
            lockStmt.setString(1, transaction.getToAccount());
            resultSet = lockStmt.executeQuery();
            if (resultSet.next()) {
                toAccount = new BankingAccount(resultSet.getString("accountId"), resultSet.getString("user"),
                        resultSet.getBigDecimal("balance"), resultSet.getBoolean("active"));
                logger.debug("transferAccountBalance to Account: " + toAccount);
            }

            // check locking status
            if (fromAccount == null || toAccount == null) {
                throw new CustomException("Fail to lock both accounts for write");
            }

            // check enough fund in source account
            BigDecimal fromAccountLeftOver = fromAccount.getBalance().subtract(transaction.getAmount());
            if (fromAccountLeftOver.compareTo(BigDecimal.ZERO) < 0) {
                throw new CustomException("Not enough capital from source Account ");
            }
            // proceed with update
            updateStmt = conn.prepareStatement(SQL_UPDATE_ACCOUNT_BALANCE);
            updateStmt.setBigDecimal(1, fromAccountLeftOver);
            updateStmt.setString(2, transaction.getFromAccount());
            updateStmt.addBatch();
            updateStmt.setBigDecimal(1, toAccount.getBalance().add(transaction.getAmount()));
            updateStmt.setString(2, transaction.getToAccount());
            updateStmt.addBatch();
            int[] rowsUpdated = updateStmt.executeBatch();
            result = rowsUpdated[0] + rowsUpdated[1];
            logger.debug("Number of rows updated for the transfer : " + result);
            // If there is no error, commit the transaction
            conn.commit();
        } catch (SQLException se) {
            // rollback transaction if exception occurs
            logger.error("Transaction Failed, rollback initiated for: " + transaction,
                    se);
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
                throw new CustomException("Fail to rollback transaction", re);
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(resultSet);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
        return result;
    }
}
