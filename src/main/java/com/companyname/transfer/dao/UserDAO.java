package com.companyname.transfer.dao;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.User;

import java.util.List;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public interface UserDAO {
    List<User> findAllActiveUsers() throws CustomException;
    User findUserById(String userId) throws CustomException;
    int insertUser(User user) throws CustomException;
    int updateUser(User user, String userId) throws CustomException;
    int deactivateUser(String userId) throws CustomException;
}

