package com.companyname.transfer.dao;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.BankingAccount;
import com.companyname.transfer.model.Transaction;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public interface BankingAccountDAO {

    int updateBalance(String accountId, BigDecimal delta) throws CustomException;
    List<BankingAccount> findAllBankingAccountsByUser(String userId) throws CustomException;
    BankingAccount findAccountById(String accountId) throws CustomException;
    int insertAccount(BankingAccount account) throws CustomException;
    int deactivateAccount(String accountId) throws CustomException;
    void deactivateAccountsByUser(String userId) throws CustomException;
    int transferAccountBalance(Transaction transaction) throws CustomException;
}

