package com.companyname.transfer.dao;

import com.companyname.transfer.config.AppConfig;
import com.companyname.transfer.dao.impl.BankingAccountDAOImpl;
import com.companyname.transfer.dao.impl.UserDAOImpl;
import org.apache.commons.dbutils.DbUtils;
import org.h2.tools.RunScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.companyname.transfer.constans.PathConstants.GENERATE_SQL_DATA_PATH;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class H2DAOFactory extends DAOFactory {
    private static Logger logger = LoggerFactory.getLogger(H2DAOFactory.class);
    private final UserDAOImpl userDAO = new UserDAOImpl();
    private final BankingAccountDAOImpl accountDAO = new BankingAccountDAOImpl();

    public H2DAOFactory() {
        DbUtils.loadDriver(AppConfig.H2DB.DRIVER);
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public BankingAccountDAO getBankingAccountDAO() {
        return accountDAO;
    }

    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager
                    .getConnection(AppConfig.H2DB.URL, AppConfig.H2DB.USER, AppConfig.H2DB.PASSWORD);
        } catch (SQLException e) {
            logger.error("Connection Failed! ", e);
        }

        if (connection != null) {
            logger.info("Connection to H2 was successfully created");
        }
        return connection;
    }

    public void generateData() {
        logger.info("Generate test data");
        Connection conn = null;
        try {
            conn = getConnection();
            RunScript.execute(conn, new FileReader(GENERATE_SQL_DATA_PATH));
        } catch (SQLException e) {
            logger.error("Generating data is failed: ", e);
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            logger.error("Generating data is failed: ", e);
            throw new RuntimeException(e);
        }
        finally {
            DbUtils.closeQuietly(conn);
        }


    }
}
