package com.companyname.transfer.dao;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public abstract class DAOFactory {

    public abstract UserDAO getUserDAO();

    public abstract BankingAccountDAO getBankingAccountDAO();

    public static DAOFactory getDAOFactory() {

        return new H2DAOFactory();

    }
    public abstract void generateData();

}
