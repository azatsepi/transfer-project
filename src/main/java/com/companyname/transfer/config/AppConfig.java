package com.companyname.transfer.config;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class AppConfig {
    public static final String HOST = System.getProperty("transfer.host", "localhost");
    public static final String PORT = System.getProperty("transfer.port", "8080");

    public static class H2DB {
        public static final String DRIVER = System.getProperty("h2db.driver", "org.h2.Driver");
        public static final String URL = System.getProperty("h2db.url", "jdbc:h2:mem:transfermoney;DB_CLOSE_DELAY=-1");
        public static final String USER = System.getProperty("h2db.user", "sa");
        public static final String PASSWORD = System.getProperty("h2db.password", "");

    }
}
