package com.companyname.transfer.exception;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class CustomException extends Exception {

	private static final long serialVersionUID = 1L;

	public CustomException(String msg) {
		super(msg);
	}

	public CustomException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
