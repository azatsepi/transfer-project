package com.companyname.transfer.exception;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class CustomErrorResponse {

    private String message;
    private int status;

    public CustomErrorResponse(String message, int status) {
        this.message = message;
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
