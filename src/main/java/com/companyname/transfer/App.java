package com.companyname.transfer;

import com.companyname.transfer.config.AppConfig;
import com.companyname.transfer.dao.H2DAOFactory;
import com.companyname.transfer.model.BankingAccount;
import com.companyname.transfer.rest.BankingAccountController;
import com.companyname.transfer.rest.ErrorHandler;
import com.companyname.transfer.rest.TransactionController;
import com.companyname.transfer.rest.UserController;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class App {
    private static Logger logger = LoggerFactory.getLogger(App.class);
    public static void main(String[] args) throws Exception {
        init();
        start();
    }

    private static void start() throws Exception {

            Server server = new Server(Integer.parseInt(AppConfig.PORT));
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setContextPath("/");
            server.setHandler(context);
            ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
            servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                UserController.class.getCanonicalName() + "," +
                        BankingAccountController.class.getCanonicalName() + "," +
                        ErrorHandler.class.getCanonicalName() + "," +
                        TransactionController.class.getCanonicalName());
            try {
                server.start();
                server.join();
            } finally {
                server.destroy();
            }
    }

    /**
     * Initialize db
     */
    private static void init() {
        logger.info("Start initialization");
        new H2DAOFactory().generateData();
        logger.info("Initialization complete");
    }
}
