package com.companyname.transfer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by zatsepilin on 26.04.2018.
 */
public class BankingAccount {

    @JsonProperty(required = true)
    private String accountId;
    @JsonProperty(required = true)
    private String user;
    @JsonProperty(required = true)
    private BigDecimal balance;
    @JsonProperty(required = true)
    private boolean active;

    public BankingAccount(String accountId, String user, BigDecimal balance, boolean active) {
        this.accountId = accountId;
        this.user = user;
        this.balance = balance;
        this.active = active;
    }

    public BankingAccount(String user, BigDecimal balance, boolean active) {
        this.user = user;
        this.balance = balance;
        this.active = active;
    }

    public BankingAccount(){}

    public String getAccountId() {
        return accountId;
    }

    public String getUser() {
        return user;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankingAccount that = (BankingAccount) o;
        return active == that.active &&
                Objects.equals(accountId, that.accountId) &&
                Objects.equals(user, that.user) &&
                Objects.equals(balance, that.balance);
    }

    @Override
    public int hashCode() {

        return Objects.hash(accountId, user, balance, active);
    }

    @Override
    public String toString() {
        return "BankingAccount{" +
                "accountId='" + accountId + '\'' +
                ", user='" + user + '\'' +
                ", balance=" + balance +
                ", active=" + active +
                '}';
    }
}
