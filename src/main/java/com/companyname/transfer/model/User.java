package com.companyname.transfer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Created by zatsepilin on 26.04.2018.
 */
public class User {

    @JsonProperty(required = true)
    private String userId;
    @JsonProperty(required = true)
    private String name;
    @JsonProperty(required = true)
    private String surName;
    @JsonProperty
    private String middleName;
    @JsonProperty(required = true)
    private String phone;
    @JsonProperty(required = true)
    private String email;
    @JsonProperty(required = true)
    private boolean active;

    public User() {

    }

    public User(String userId, String name, String surName, String middleName, String phone, String email, boolean active) {
        this.userId = userId;
        this.name = name;
        this.surName = surName;
        this.middleName= middleName;
        this.phone = phone;
        this.email = email;
        this.active = active;
    }

    public User(String name, String surName, String middleName, String phone, String email, boolean active) {
        this.name = name;
        this.surName = surName;
        this.middleName= middleName;
        this.phone = phone;
        this.email = email;
        this.active = active;
    }


    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return active == user.active &&
                Objects.equals(userId, user.userId) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surName, user.surName) &&
                Objects.equals(middleName, user.middleName) &&
                Objects.equals(phone, user.phone) &&
                Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, name, surName, middleName, phone, email, active);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", middleName='" + middleName+ '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", active=" + active +
                '}';
    }
}
