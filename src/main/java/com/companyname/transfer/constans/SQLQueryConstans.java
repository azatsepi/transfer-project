package com.companyname.transfer.constans;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class SQLQueryConstans {

    public static class Account {
        public final static String SQL_GET_ACCOUNT_BY_ID = "SELECT  accountId, user, balance, active FROM BankingAccount WHERE accountId = ? AND active = TRUE";
        public final static String SQL_GET_ACCOUNTS_BY_USER = "SELECT  accountId, user, balance, active FROM BankingAccount WHERE user = ? AND active = TRUE";
        public final static String SQL_LOCK_ACCOUNT_BY_ID = "SELECT accountId, user, balance, active  FROM BankingAccount WHERE accountId = ? AND active = TRUE FOR UPDATE";
        public final static String SQL_CREATE_ACCOUNT = "INSERT INTO BankingAccount (user, balance, active) VALUES (?, ?, true)";
        public final static String SQL_UPDATE_ACCOUNT_BALANCE = "UPDATE BankingAccount SET balance = ? WHERE accountId = ? AND active = TRUE";
        public final static String SQL_DEACTIVATE_ACCOUNT_BY_ID = "UPDATE BankingAccount SET active = false WHERE accountId = ? AND active = true";
        public final static String SQL_DEACTIVATE_ACCOUNTS_BY_USER = "UPDATE BankingAccount SET active = false WHERE user = ? AND active = true";
    }

    public static class User {
        public final static String SQL_GET_USER_BY_ID = "SELECT userId, name, surName, middleName, phone, email, active FROM User WHERE userId = ? AND active = true";
        public final static String SQL_GET_ALL_USERS = "SELECT userId, name, surName, middleName, phone, email, active FROM User where active = true";
        public final static String SQL_INSERT_USER = "INSERT INTO User (name, surName, middleName, phone, email, active) VALUES (?, ?, ?, ?, ?, true )";
        public final static String SQL_UPDATE_USER = "UPDATE User SET name = ?, surName = ?, middleName= ?, phone = ?, email = ? WHERE userId = ? AND active = true";
        public final static String SQL_DEACTIVATE_USER = "UPDATE User SET active = false WHERE userId = ? AND active = true";
    }
}
