package com.companyname.transfer.rest.validation;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class UserValidation extends Validation {

    private static final Logger logger = LoggerFactory.getLogger(UserValidation.class);
    public boolean validate(User user) throws CustomException {
        logger.info("validate");
        ValidationStatus status = new ValidationStatus();
        validateStringField(user.getName(), status);
        validateStringField(user.getSurName(), status);
        validateStringField(user.getEmail(), status);
        validateStringField(user.getPhone(), status);

        return status.isSuccessful();

    }


}
