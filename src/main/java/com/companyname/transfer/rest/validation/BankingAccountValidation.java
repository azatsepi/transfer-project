package com.companyname.transfer.rest.validation;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.BankingAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class BankingAccountValidation extends Validation {
    private static final Logger logger = LoggerFactory.getLogger(BankingAccountValidation.class);
    public boolean validate(BankingAccount account) throws CustomException {
        logger.info("validate");
        ValidationStatus status = new ValidationStatus();
        validateStringField(account.getUser(), status);
        validateDecimalField(account.getBalance(), status);
        return status.isSuccessful();

    }
}
