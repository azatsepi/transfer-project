package com.companyname.transfer.rest.validation;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class ValidationStatus {
        private boolean successful = true;

        public boolean isSuccessful() {
            return successful;
        }

        public void setSuccessful(boolean successful) {
            this.successful = successful;
        }
    }

