package com.companyname.transfer.rest.validation;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by zatsepilin on 28.04.2018.
 */
public class Validation {
    private static final Logger logger = LoggerFactory.getLogger(Validation.class);
    protected void validateStringField(String field, ValidationStatus status) {
        if (StringUtils.isEmpty(field)) {
            logger.warn("String field is empty!!!");
            status.setSuccessful(false);
        }
    }

    protected void validateDecimalField(BigDecimal field, ValidationStatus status) {
        if (field == null) {
            logger.warn("Decimal field is empty!!!");
            status.setSuccessful(false);
        }
    }
}
