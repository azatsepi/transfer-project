package com.companyname.transfer.rest;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.BankingAccount;
import com.companyname.transfer.rest.validation.BankingAccountValidation;
import com.companyname.transfer.services.BankingAccountService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Created by zatsepilin on 28.04.2018.
 */
@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class BankingAccountController {
    private static final Logger logger = LoggerFactory.getLogger(BankingAccountController.class);

    /**
     * Find all users.
     *
     * @return list of accounts
     * @throws CustomException
     */
    @GET
    @Path("/{userId}/all")
    public List<BankingAccount> getAllBankingAccount(@PathParam("userId") String userId) throws CustomException {
        return new BankingAccountService().getAllAccounts(userId);
    }

    /**
     * Find user by ID.
     *
     * @param accId accId
     * @return account
     * @throws CustomException
     */
    @GET
    @Path("/{accountId}")
    public BankingAccount getBankingAccountById(@PathParam("accountId") String accId) throws CustomException {
        final BankingAccount account = new BankingAccountService().getAccountById(accId);
        if (account == null) {
            throw new WebApplicationException("Account not Found", Response.Status.NOT_FOUND);
        }
        return account;
    }
    /**
     * Find balance by account Id
     * @param accountId
     * @return
     * @throws CustomException
     */
    @GET
    @Path("/{accountId}/balance")
    public BigDecimal getBalance(@PathParam("accountId") String accountId) throws CustomException {
        final BankingAccount account = new BankingAccountService().getAccountById(accountId);

        if(account == null){
            throw new WebApplicationException("Account not found", Response.Status.NOT_FOUND);
        }
        return account.getBalance();
    }

    /**
     * Create new user.
     *
     * @param account account model
     * @return response
     * @throws CustomException
     */
    @POST
    @Path("/create")
    public Response createBankingAccount(BankingAccount account) throws CustomException {
        if (!(new BankingAccountValidation().validate(account))) {
            throw new WebApplicationException("User is invalid", Response.Status.BAD_REQUEST);
        }
        int createdRow =  new BankingAccountService().createAccount(account);
        return (createdRow == 1 ? Response.status(Response.Status.OK).build() : Response.status(Response.Status.NOT_FOUND).build());
    }

    /**
     * Deposit amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws CustomException
     */
    @PUT
    @Path("/{accountId}/increase/{amount}")
    public BankingAccount deposit(@PathParam("accountId") String accountId, @PathParam("amount") BigDecimal amount) throws CustomException {
        if(StringUtils.isEmpty(accountId)) {
            throw new WebApplicationException("accountId is empty", Response.Status.BAD_REQUEST);
        }
        if (amount.compareTo(BigDecimal.ZERO) <=0){
            throw new WebApplicationException("Invalid amount", Response.Status.BAD_REQUEST);
        }
        BankingAccountService service = new BankingAccountService();
        service.updateAccountBalance(accountId,amount.setScale(4, RoundingMode.HALF_EVEN));
        return service.getAccountById(accountId);
    }

    /**
     * Withdraw amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws CustomException
     */
    @PUT
    @Path("/{accountId}/decrease/{amount}")
    public BankingAccount withdraw(@PathParam("accountId") String accountId,@PathParam("amount") BigDecimal amount) throws CustomException {

        if (amount.compareTo(BigDecimal.ZERO) <=0){
            throw new WebApplicationException("Invalid amount", Response.Status.BAD_REQUEST);
        }
        BigDecimal delta = amount.negate();
        logger.debug("Withdraw service: delta change to account  " + delta + " Account ID = " +accountId);
        BankingAccountService service = new BankingAccountService();
        service.updateAccountBalance(accountId,delta.setScale(4, RoundingMode.HALF_EVEN));
        return service.getAccountById(accountId);
    }

    /**
     * Deactivate user
     *
     * @param accountId userId
     * @return response
     * @throws CustomException
     */
    @DELETE
    @Path("/{accountId}")
    public Response deleteAccount(@PathParam("accountId") String accountId) throws CustomException {
        if(StringUtils.isEmpty(accountId)) {
            throw new WebApplicationException("userId is empty", Response.Status.BAD_REQUEST);
        }
        int updatedCount = new BankingAccountService().deactivateAccount(accountId);
        return (updatedCount == 1 ? Response.status(Response.Status.OK).build() : Response.status(Response.Status.NOT_FOUND).build());
    }
}
