package com.companyname.transfer.rest;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.Transaction;
import com.companyname.transfer.services.TransactionService;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by zatsepilin on 28.04.2018.
 */
@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionController {

    @POST
    public Response transferCapital(Transaction transaction) throws CustomException {

        int updateCount = new TransactionService().transferAccountBalance(transaction);
        if (updateCount == 2) {
            return Response.status(Response.Status.OK).build();
        } else {
            // transaction failed
            throw new WebApplicationException("Transaction failed", Response.Status.BAD_REQUEST);
        }
    }
}
