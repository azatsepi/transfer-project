package com.companyname.transfer.rest;

import com.companyname.transfer.exception.CustomException;
import com.companyname.transfer.model.User;
import com.companyname.transfer.rest.validation.UserValidation;
import com.companyname.transfer.services.UserService;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by zatsepilin on 28.04.2018.
 */
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserController {


    /**
     * Find all users.
     *
     * @return list of users
     * @throws Exception
     */
    @GET
    @Path("/all")
    public List<User> getAllUsers() throws CustomException {
        return new UserService().getAllUsers();
    }

    /**
     * Find user by ID.
     *
     * @param userId userId
     * @return list of user
     * @throws Exception
     */
    @GET
    @Path("/{userId}")
    public User getUserById(@PathParam("userId") String userId) throws CustomException {
        final User user = new UserService().getUserById(userId);
        if (user == null) {
            throw new WebApplicationException("User Not Found", Response.Status.NOT_FOUND);
        }
        return user;
    }


    /**
     * Create new user.
     *
     * @param user user model
     * @return response
     * @throws CustomException
     */
    @POST
    @Path("/create")
    public Response createUser(User user) throws CustomException {
        if (!(new UserValidation().validate(user))) {
            throw new WebApplicationException("User is invalid", Response.Status.BAD_REQUEST);
        }
        int createdCount = new UserService().createUser(user);
        return (createdCount == 1 ? Response.status(Response.Status.OK).build() : Response.status(Response.Status.BAD_REQUEST).build());
    }

    /**
     * Update user
     *
     * @param userId userID
     * @param user   user model
     * @return response
     * @throws CustomException
     */
    @PUT
    @Path("/{userId}")
    public Response updateUser(@PathParam("userId") String userId, User user) throws CustomException {
        if(StringUtils.isEmpty(userId)) {
            throw new WebApplicationException("userId is empty", Response.Status.BAD_REQUEST);
        }
        if (!(new UserValidation().validate(user)) ) {
            throw new WebApplicationException("user is invalid", Response.Status.BAD_REQUEST);
        }
        int updatedCount =  new UserService().updateUser(user, userId);
        return (updatedCount == 1 ? Response.status(Response.Status.OK).build() : Response.status(Response.Status.NOT_FOUND).build());
    }

    /**
     * Deactivate user
     *
     * @param userId userId
     * @return response
     * @throws CustomException
     */
    @DELETE
    @Path("/{userId}")
    public Response deleteUser(@PathParam("userId") String userId) throws CustomException {
        if(StringUtils.isEmpty(userId)) {
            throw new WebApplicationException("userId is empty", Response.Status.BAD_REQUEST);
        }
        int updatedCount = new UserService().deactivateUser(userId);
        return (updatedCount == 1 ? Response.status(Response.Status.OK).build() : Response.status(Response.Status.NOT_FOUND).build());
    }
}
