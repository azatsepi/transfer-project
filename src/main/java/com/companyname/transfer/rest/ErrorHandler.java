package com.companyname.transfer.rest;

import com.companyname.transfer.exception.CustomErrorResponse;
import com.companyname.transfer.exception.CustomException;

import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by zatsepilin on 28.04.2018.
 */
@Provider
public class ErrorHandler implements ExceptionMapper<CustomException> {

    private static final Logger logger = LoggerFactory.getLogger(ErrorHandler.class);

    public Response toResponse(CustomException exception) {

        CustomErrorResponse customErrorResponse = new CustomErrorResponse(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR_500);
        logger.error("ERROR: ", exception);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(customErrorResponse).type(MediaType.APPLICATION_JSON).build();
    }
}
